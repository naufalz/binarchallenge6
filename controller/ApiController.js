const models = require('../models');
const User = require('./UserController');
const UserGame = models.user_game;
const UserGameBiodata = models.user_game_biodata;
const UserGameHistory = models.user_game_history;

class ApiController {
    static user_data(req, res, next) {
        // https://medium.com/@eth3rnit3/sequelize-relationships-ultimate-guide-f26801a75554
        UserGame.findAll({
            include: 'user_game_biodata'
        })
            .then(user_game => {
                res.status(200).json({
                    success: true,
                    data: user_game,
                })
            })
            .catch(err => {
                res.status(200).json({
                    success: false,
                    error: err,
                })
            });
    }

    static user_data_one(req, res, next) {
        const { username } = req.params
        UserGame.findOne({
            where: {
                username: username,
            },
            include: ['user_game_biodata', 'user_game_history']
        })
            .then(user_game => {
                res.status(200).json({
                    success: true,
                    data: user_game,
                })
            })
            .catch(err => {
                res.status(200).json({
                    success: false,
                    error: err,
                })
            });
    }

    static user_register(req, res, next) {
        const { password, username, name } = req.body

        if (!password || !username || !name) {
            return res.status(200).json({
                success: false,
                message: "Data tidak lengkap",
            })
        }

        UserGame.create({
            username: username,
            password: password,
        })
            .then((newCompany) => {

                UserGameBiodata.create({
                    user_game_id: newCompany.get().id,
                    name: name,
                })
                    .then((newCompany) => {
                        res.status(200).json({
                            success: true,
                            message: "User baru berhasil terdaftar",
                        })
                    })
                    .catch((err) => {
                        res.status(200).json({
                            success: false,
                            message: "Username sudah digunakan",
                        })
                    })

            })
            .catch((err) => {
                res.status(200).json({
                    success: false,
                    message: "Username sudah digunakan",
                })
            })
    }

    static user_data_update(req, res, next) {
        const { username } = req.params
        const { name } = req.body

        if (!name) {
            return res.status(200).json({
                success: false,
                message: "Data tidak lengkap",
            })
        }

        UserGame.findOne({
            where: {
                username: username,
            },
            include: 'user_game_biodata'
        })
            .then(user_game => {
                if (user_game) {
                    UserGameBiodata.update(
                        {
                            name: name
                        },
                        {
                            where: {
                                user_game_id: user_game.id
                            }
                        }
                    )
                        .then(callback =>
                            res.status(200).json({
                                success: true,
                                message: 'Update berhasil'
                            })
                        )
                } else {
                    res.status(200).json({
                        success: false,
                        message: 'User tidak ditemukan',
                    })
                }
            })
            .catch(err => {
                res.status(200).json({
                    success: false,
                    error: err,
                })
            });
    }

    static user_data_delete(req, res, next) {
        const { username } = req.params

        if (!username) {
            return res.status(200).json({
                success: false,
                message: "Data tidak lengkap",
            })
        }

        UserGame.destroy({
            where: {
                username: username,
            }
        })
            .then((callback) => {
                if (callback) {
                    res.status(200).json({
                        success: true,
                        message: 'User berhasil dihapus',
                    })
                } else {
                    res.status(200).json({
                        success: false,
                        message: 'User tidak ditemukan',
                    })
                }
            })
            .catch(err => {
                res.status(200).json({
                    success: false,
                    error: err,
                })
            });
    }

    static record_history(req, res, next) {
        const { id, score } = req.body
        UserGameHistory.create({
            user_game_id: id,
            score: score,
        })
            .then((newCompany) => {
                res.status(200).json({
                    success: true,
                    message: "History berhasil tercatat",
                })
            })
            .catch((err) => {
                res.status(200).json({
                    success: false,
                    message: "Gagal mencatat History",
                })
            })
    }
}

module.exports = ApiController