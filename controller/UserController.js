const user = require("../model/user.json")
// const UserGame = require("../models/user_game_biodata");
const UserModel = require("../model/user.js")
const fs = require('fs')

const models = require('../models');
const UserGame = models.user_game;
const UserGameBiodata = models.user_game_biodata;


class User {
  static loginPage(req, res, next) {
    res.render("login")
  }

  static login(req, res, next) {
    const { password, username } = req.body
    UserGame.findOne({
      where: {
        username: username,
        password: password,
      },
      include: 'user_game_biodata'
    })
      .then(user_game => {
        if (user_game) {
          const _id = user_game.id
          const _username = user_game.username
          const _name = user_game.user_game_biodata.name
          req.session.login = true;
          req.session.user_id = _id;
          req.session.username = _username;
          req.session.name = _name;

          res.status(200).json({
            success: true,
            username: _username,
          })
        } else {
          res.status(200).json({
            success: false,
            message: "username or password is wrong",
          })
        }
      })
      .catch(err => {
        throw {
          status: 200,
          success: false,
          message: err,
        }
      });

  }

  static logout(req, res, next) {
    req.session.login = false;
    req.session.user_id = null;
    req.session.username = null;
    req.session.name = null;
    res.redirect('/');
  }

  static registerPage(req, res, next) {
    res.render("register")
  }

  static register(req, res, next) {
    const { password, username, name } = req.body

    UserGame.create({
      username: username,
      password: password,
    })
      .then((newCompany) => {

        UserGameBiodata.create({
          user_game_id: newCompany.get().id,
          name: name,
        })
          .then((newCompany) => {
            res.status(200).json({
              success: true,
            })
          })
          .catch((err) => {
            res.status(200).json({
              success: false,
              message: "Username sudah digunakan",
            })
          })

      })
      .catch((err) => {
        res.status(200).json({
          success: false,
          message: "Username sudah digunakan",
        })
      })
  }
}


module.exports = User