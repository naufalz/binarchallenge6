const route = require("express").Router()
const { Home, User, ApiController, AccountController } = require("../controller")
const Middleware = require("../middleware")


// Route
route.get("/", Home.home)
route.get("/login", Middleware.doWithoutAuth, User.loginPage)
route.post("/login", Middleware.doWithoutAuth, User.login)
route.get("/register", Middleware.doWithoutAuth, User.registerPage)
route.post("/register", Middleware.doWithoutAuth, User.register)
route.get("/logout", Middleware.doWithAuth, User.logout)
route.get("/game", Middleware.doWithAuth, Home.game)
route.get("/account/history", Middleware.doWithAuth, AccountController.history)

// Just API
// Untuk penggunaaan API baca pada README.md
route.get("/api/data/user", ApiController.user_data)
route.post("/api/data/user/add", ApiController.user_register)
route.get("/api/data/user/:username", ApiController.user_data_one)
route.post("/api/data/user/:username/update", ApiController.user_data_update)
route.delete("/api/data/user/:username/delete", ApiController.user_data_delete)

route.post("/api/history/record", ApiController.record_history)

module.exports = route