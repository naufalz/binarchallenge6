'use strict';

module.exports = {


  up: async (queryInterface) => {
    await queryInterface.bulkInsert('user_games', [
      { username: 'admin', password: 'password' },
      { username: 'user', password: 'password' }
      { username: 'naufalzaky', password: 'password' },
    ], {});

    const user_game = await queryInterface.sequelize.query(
      `SELECT id from user_games;`
    );

    const courseRows = user_game[0];

    return await queryInterface.bulkInsert('user_game_biodata', [
      { name: 'Admin1', email: 'admin1@gmail.com', user_game_id: courseRows[0].id },
      { name: 'Dummy1', email: 'dummy1@gmail.com', user_game_id: courseRows[1].id },
      { name: 'Harlan SR', email: 'harlan@gmail.com', user_game_id: courseRows[2].id },
    ], {});
  },

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('user_game_biodata', null, {});
    await queryInterface.bulkDelete('user_games', null, {});
  }
};
