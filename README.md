# BINAR - CHALLENGE 6
Ini adalah challenge binar chapter 6 tentang Node.js dan lainnya

## Apa yang baru?
Project ini adalah pengembangan dari chapter 5, jadi ada beberapa perkembangan pada chapter ini
1. Menggunakan sequelize
1. Menggunakan database postgest
1. Menambahkan database Migration
1. Menambahkan database Model
1. Menambahkan database Seeder
1. Menambahkan fungsi ``yarn db-fresh`` untuk melakukan resete database + seeder
1. Login dan Register sudah menggunakan database
1. Permainan sudah tercatat pada History game
1. History game sudah bisa dilihat melalui link `/account/history`

## Available account
#### username: ```user```
#### password: ```password```

## Fungsi Yarn yang bisa digunakan
1. Menjalankan Website
```
yarn start
```
2. Resete semua database dan melakukan seeder
```
yarn db-fresh
```

## API Yang bisa digunakan
1. Melihat semua data User
```
GET /api/data/user
```

2. Melihat detail User
```
GET /api/data/user/{username}
```

3. Menambah User baru

```
POST /api/data/user/add

tambahkan beberapa Body
- name
- username
- password
```

4. Update data user
```
POST /api/data/user/{username}/update

tambahkan beberapa Body
- name
```

5. Hapus data user
```
DELETE /api/data/user/{username}/delete
```

6. Record History Game
```
POST /api/history/record

tambahkan beberapa body
- id
- score
```


